﻿using System.Collections;
using UnityEngine;

public class Pool : MonoBehaviour
{
    private int pool_count = 13;

    public GameObject prefab;

    public Transform pool_parent;

    private int pool_element = 0;

    private GameObject[] pool_array;

    public float TimeSpawn;


    private void Start()
    {
        CreatePool();
        StartCoroutine(pool());
    }

    public void CreatePool()
    {
        pool_array = new GameObject[pool_count];
        for (int i = 0; i < pool_count; i++)
        {
            pool_array[i] = Instantiate(prefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity, pool_parent);
            pool_array[i].SetActive(false);
        }
    }

    private IEnumerator pool()
    {
        while (PlayerMove.alive == true)
        {
            GameObject obj = pool_parent.GetChild(pool_element).gameObject;
            obj.SetActive(true);
            obj.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            pool_element++;
            if (pool_element > pool_parent.childCount - 1)
            {
                pool_element = 0;
            }
            yield return new WaitForSeconds(TimeSpawn);
        }
    }
}
