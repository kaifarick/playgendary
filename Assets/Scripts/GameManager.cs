﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject DeathScreen;
    public GameObject GameScreen;
    public GameObject AudioM;
    public GameObject soundBtn;
    public GameObject GodModBtn;
    public PlayerMove player;

    public Text PointsPlay;
    public Text PointsDeath;
    public  Text coinsPlay;
    public Text coinsDeath;
    public Text TopCoinsDeath;

    public int coins;
    public int points;
    public int TopPoints;

    public bool SoundState;

    private void Start()
    {
        AddCoins(0);
        AddPoints(0);
        if(SoundState == false)
        {
            AudioOff();
        }
    }

    public void ShowResult()
    {
        GameScreen.SetActive(false);
        DeathScreen.SetActive(true);
        SaveManager.Instance.SaveGame();
    }
    public void Restart()
    {
        SceneManager.LoadScene("Main");
    }
    public void AddCoins(int number)
    {
        coins += number;
        coinsPlay.text = coins.ToString();
        coinsDeath.text = coins.ToString();
    }
    public void AddPoints(int number)
    {
        points += number;
        PointsPlay.text = points.ToString();
        PointsDeath.text = points.ToString();
    }
    public void RefreshText()
    {
        coinsPlay.text = coinsPlay.ToString();
        coinsDeath.text = coinsDeath.ToString();
        PointsDeath.text = coinsDeath.ToString();
        PointsPlay.text = coinsPlay.ToString();
        TopCoinsDeath.text = "Рекорд " + TopPoints.ToString();
        CheckMaxPoints();
    }
    public void CheckMaxPoints()
    {
        if (points > TopPoints)
        {
            TopPoints = points;
            TopCoinsDeath.text = "Рекорд " + TopPoints.ToString();
            SaveManager.Instance.SaveGame();
        }
    }
    public void AudioOn()
    {
        AudioM.SetActive(true);
        soundBtn.SetActive(false);
        SoundState = true ;
        SaveManager.Instance.SaveGame();
    }
    public void AudioOff()
    {
        AudioM.SetActive(false);
        soundBtn.SetActive(true);
        SoundState = false;
        SaveManager.Instance.SaveGame();  
    }

    public void GodModOn()
    {
        GodModBtn.SetActive(false);
        player.GetComponent<BoxCollider>().enabled = false;
        player.GetComponent<Rigidbody>().useGravity = false;
    }

    public void GodModOff()
    {
        GodModBtn.SetActive(true);
        player.GetComponent<BoxCollider>().enabled = true;
        player.GetComponent<Rigidbody>().useGravity = true;
    }
}
