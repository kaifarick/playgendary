﻿using UnityEngine;

public class TouchStart : MonoBehaviour
{
    public GameObject StartSceen;
    public GameObject GameScreen;

    void Update()
    {
        FirstTouch();   
    }
    public void FirstTouch()
    {
        if((Input.touchCount > 0)||Input.GetKeyUp("w"))
        {
            StartSceen.SetActive(false);
            GameScreen.SetActive(true);
            Destroy(gameObject);
        }
    }
}
