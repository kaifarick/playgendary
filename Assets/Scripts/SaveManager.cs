﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public GameManager GM;
    string filePath;

    public static SaveManager Instance;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        GM = FindObjectOfType<GameManager>();
        filePath = Application.persistentDataPath + "data.savegame";

        LoadGame();
        SaveGame();
    }

public void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = new FileStream(filePath, FileMode.Create);

        Save save = new Save();
        save.Coins = GM.coins;
        save.MaxPoints = GM.TopPoints;
        save.Sound = GM.SoundState;

        bf.Serialize(fs, save);
        fs.Close();
    }

    void LoadGame()
    {
        if (!File.Exists(filePath))
            return;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = new FileStream(filePath, FileMode.Open);

        Save save = (Save)bf.Deserialize(fs);

        GM.coins = save.Coins;
        GM.TopPoints = save.MaxPoints;
        GM.SoundState = save.Sound;

        fs.Close();

        GM.RefreshText();
    }

    [System.Serializable]
    public class Save
    {
        public int Coins;
        public int MaxPoints;
        public bool Sound;
    }
}
