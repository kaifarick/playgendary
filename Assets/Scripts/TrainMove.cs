﻿using DG.Tweening;
using UnityEngine;

public class TrainMove : MoveObject
{
    public AudioSource Train;
    public AudioClip TrainClip;

    void OnEnable()
    {
        ObjectTransform();
        Train.PlayOneShot(TrainClip, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        ObjectOff();
    }
}
