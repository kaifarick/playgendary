﻿using System.Collections.Generic;
using UnityEngine;

public class RoadSpawner : MonoBehaviour
{
    public GameObject[] RoadBlockPrefabs;
    public GameObject StartBlock;

    float blockZPos = 0;
    int blockCount = 4;
    float blockLenght = 0;
    float safeZone = 50f;

    public Transform PlayerTransf;
    List<GameObject> CurrentBlocks = new List<GameObject>();
    void Start()
    {
        StartSpawn();
    }

    void StartSpawn()
    {
        blockZPos = StartBlock.transform.position.z;
        blockLenght = StartBlock.GetComponent<BoxCollider>().bounds.size.z;

        CurrentBlocks.Add(StartBlock);

        for (int i = 0; i < blockCount; i++)
            SpawnBlock();
    }
    void CheckForSpawn()
    {       
        if (PlayerTransf.position.z - safeZone> (blockZPos - blockCount * blockLenght))
        {
            SpawnBlock();
            DestroyBlock();
        }
     }
    void Update()
    {
        CheckForSpawn();
    }
    void SpawnBlock()
    {
        GameObject block = Instantiate(RoadBlockPrefabs[Random.Range(0, RoadBlockPrefabs.Length)], transform);

        blockZPos += blockLenght;

        block.transform.position = new Vector3(0, 0, blockZPos);

        CurrentBlocks.Add(block);
    }

    void DestroyBlock()
    {
        Destroy(CurrentBlocks[0]);
        CurrentBlocks.RemoveAt(0);
    }
}
