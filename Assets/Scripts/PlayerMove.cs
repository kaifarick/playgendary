﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class PlayerMove : MonoBehaviour

{
    public float JumpDistance;
    public float DeathTime;
    public static bool alive;

    public GameManager GM;
    public AudioManager audioManager;
    private Animator anim;
    private void Start()
    {
        alive = true;
        anim = GetComponent<Animator>();

        //StartCoroutine(StayDeath());
    }
    private void Update()
    {
        ButtonMove();
    }

    void ButtonMove()
    {
        anim.SetBool("Jump", false);
        Vector3 rayPosition = new Vector3(transform.position.x, transform.position.y + 4f, transform.position.z);
        RaycastHit hit;

        Vector3 JumpVector = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Ray JumpRay = new Ray(JumpVector, -transform.up);
        RaycastHit JumpHit;
        Physics.Raycast(JumpRay, out JumpHit);

        Debug.DrawRay(JumpVector,-transform.up);
        Debug.Log(JumpHit.distance);


        if (Input.GetKeyUp("w") && JumpHit.distance == 0 && alive == true)
        {
            Ray ray = new Ray(rayPosition, transform.forward);
            Physics.Raycast(ray, out hit);
            if (hit.distance >= 3f || hit.collider == null)
            {
                anim.SetBool("Jump", true);
                transform.rotation = Quaternion.Euler(0,0,0);
                transform.DOLocalJump(new Vector3(transform.position.x, transform.position.y, transform.position.z + JumpDistance), 0.2f, 1, 0.1f, false);
                GM.AddPoints(1);
            }
        }

        else if (Input.GetKeyUp("s") && JumpHit.distance == 0 && alive == true)
        {
            Ray ray = new Ray(rayPosition, -transform.forward);
            Physics.Raycast(ray, out hit);
            if (hit.distance >= 3f || hit.collider == null)
            {
                anim.SetBool("Jump", true);
                transform.rotation = Quaternion.Euler(0, 180, 0);
                transform.DOLocalJump(new Vector3(transform.position.x, transform.position.y, transform.position.z - JumpDistance), 0.2f, 1, 0.2f, true);
            }
        }

        else if (Input.GetKeyUp("d") && JumpHit.distance == 0 && alive == true)
        {
            Ray ray = new Ray(rayPosition, transform.right);
            Physics.Raycast(ray, out hit);
            if (hit.distance >= 3f || hit.collider == null)
            {
                anim.SetBool("Jump", true);
                transform.rotation = Quaternion.Euler(0, 90, 0);
                transform.DOLocalJump(new Vector3(transform.position.x + JumpDistance, transform.position.y, transform.position.z), 0.2f, 1, 0.2f, true);
            }
        }

        else if (Input.GetKeyUp("a") && JumpHit.distance == 0 && alive == true)
        {
            Ray ray = new Ray(rayPosition, -transform.right);
            Physics.Raycast(ray, out hit);
            if (hit.distance >= 3f || hit.collider == null)
            {
                anim.SetBool("Jump", true);
                transform.rotation = Quaternion.Euler(0, -90, 0);
                transform.DOLocalJump(new Vector3(transform.position.x - JumpDistance, transform.position.y, transform.position.z), 0.2f, 1, 0.2f, true);
            }
        }
    }

    void SwipeMove()
    {
        anim.SetBool("Jump", false);
        Vector3 rayPosition = new Vector3(transform.position.x, transform.position.y + 4f, transform.position.z);
        RaycastHit hit;

        Vector3 JumpVector = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        RaycastHit JumpHit;
        Ray JumpRay = new Ray(JumpVector, -transform.up);
        Physics.Raycast(JumpRay, out JumpHit);
        if (Input.touchCount > 0)
        {
            Vector2 delta = Input.GetTouch(0).deltaPosition;

            if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
            {
                if (delta.x > 0)
                {
                    anim.SetBool("Jump", true);
                    transform.rotation = Quaternion.Euler(0, 90, 0);
                    transform.DOLocalJump(new Vector3(transform.position.x + JumpDistance, transform.position.y, transform.position.z), 1f, 1, 0.4f, true);
                }
                else if(delta.x < 0)
                {
                    anim.SetBool("Jump", true);
                    transform.rotation = Quaternion.Euler(0, -90, 0);
                    transform.DOLocalJump(new Vector3(transform.position.x - JumpDistance, transform.position.y, transform.position.z), 1f, 1, 0.4f, true);
                }
            }
            else if(Mathf.Abs(delta.x)<Mathf.Abs(delta.y))
            {
                if(delta.y > 0)
                {
                    anim.SetBool("Jump", true);
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    transform.DOLocalJump(new Vector3(transform.position.x, transform.position.y, transform.position.z + JumpDistance), 1f, 1, 0.4f, true);
                    GM.AddPoints(1);
                }
                else if(delta.y < 0)
                {
                    anim.SetBool("Jump", true);
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                    transform.DOLocalJump(new Vector3(transform.position.x, transform.position.y, transform.position.z - JumpDistance), 1f, 1, 0.4f, true);
                }
            }
        }
    }
    void Death()
    {
        anim.SetTrigger("Dead");
        alive = false;
        GM.ShowResult();
        GM.CheckMaxPoints();
        audioManager.CarSound.Stop();
    }
    private void OnTriggerEnter(Collider other)
    {
        Coin coin = other.GetComponent<Coin>();
        if (other.gameObject.CompareTag("obstacle"))
        {
            Death();
        }
        else if (coin)
        {
            GM.AddCoins(1);
            Destroy(other.gameObject);
        }
    }
    private void OnCollisionStay(Collision collision)
    {
            if (collision.gameObject.CompareTag("Boat"))
        {
            transform.position = collision.transform.position;
        
        }
    }
    IEnumerator StayDeath()
    {
        while (true)
        {
            float playerPosition = transform.position.z;
            yield return new WaitForSeconds(DeathTime);
            if (playerPosition >= transform.position.z)
            {
                Death();
            }
            yield return new WaitForSeconds(1);
        }
    }    
}



