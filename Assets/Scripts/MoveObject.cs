﻿using UnityEngine;
using DG.Tweening;

public class MoveObject : MonoBehaviour
{
    public float duration;

    public void ObjectOff()
    {
        if (transform.position.x >= 30f)
        gameObject.SetActive(false);
    }

    public void ObjectTransform()
    {
        if (PlayerMove.alive == true)
            transform.DOLocalMoveX(80f, duration, false);
    }
}
