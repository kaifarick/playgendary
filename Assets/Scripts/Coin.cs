﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    Transform transformCoin
    {
        get
        {
           return GetComponent<Transform>();
        }
    }
    Vector3 vector = new Vector3(10, 0, 0);

    void Update()
    {
        transformCoin.Rotate(vector);
    }
}
